<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ExchangeRateRepository")
 */
class ExchangeRate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currenySource;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currencyDestination;

    /**
     * @ORM\Column(type="decimal",precision=19, scale=8, nullable=true)
     */
    private $rate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validFrom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $validTo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrenySource(): ?int
    {
        return $this->currenySource;
    }

    public function setCurrenySource(?int $currenySource): self
    {
        $this->currenySource = $currenySource;

        return $this;
    }

    public function getCurrencyDestination(): ?int
    {
        return $this->currencyDestination;
    }

    public function setCurrencyDestination(?int $currencyDestination): self
    {
        $this->currencyDestination = $currencyDestination;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(?int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getValidFrom(): ?\DateTimeInterface
    {
        return $this->validFrom;
    }

    public function setValidFrom(?\DateTimeInterface $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    public function getValidTo(): ?\DateTimeInterface
    {
        return $this->validTo;
    }

    public function setValidTo(?\DateTimeInterface $validTo): self
    {
        $this->validTo = $validTo;

        return $this;
    }
}
