<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SurchargeRepository")
 */
class Surcharge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currencyId;

    /**
     * @ORM\Column(type="decimal",precision=19, scale=8, nullable=true)
     */
    private $surchage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrencyId(): ?int
    {
        return $this->currencyId;
    }

    public function setCurrencyId(?int $currencyId): self
    {
        $this->currencyId = $currencyId;

        return $this;
    }

    public function getSurchage(): ?float
    {
        return $this->surchage;
    }

    public function setSurchage(?int $surchage): self
    {
        $this->surchage = $surchage;

        return $this;
    }
}
