<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyOrderRepository")
 */
class CurrencyOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currencyId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rateId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $surchargePercentage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currencyAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $payoutAmount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $surchargeAmount;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCreated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCurrencyId(): ?int
    {
        return $this->currencyId;
    }

    public function setCurrencyId(?int $currencyId): self
    {
        $this->currencyId = $currencyId;

        return $this;
    }

    public function getRateId(): ?int
    {
        return $this->rateId;
    }

    public function setRateId(?int $rateId): self
    {
        $this->rateId = $rateId;

        return $this;
    }

    public function getSurchargePercentage(): ?int
    {
        return $this->surchargePercentage;
    }

    public function setSurchargePercentage(?int $surchargePercentage): self
    {
        $this->surchargePercentage = $surchargePercentage;

        return $this;
    }

    public function getCurrencyAmount(): ?int
    {
        return $this->currencyAmount;
    }

    public function setCurrencyAmount(?int $currencyAmount): self
    {
        $this->currencyAmount = $currencyAmount;

        return $this;
    }

    public function getPayoutAmount(): ?int
    {
        return $this->payoutAmount;
    }

    public function setPayoutAmount(?int $payoutAmount): self
    {
        $this->payoutAmount = $payoutAmount;

        return $this;
    }

    public function getSurchargeAmount(): ?int
    {
        return $this->surchargeAmount;
    }

    public function setSurchargeAmount(?int $surchargeAmount): self
    {
        $this->surchargeAmount = $surchargeAmount;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }
}
