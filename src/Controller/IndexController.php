<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\Entity\Currency;
use App\Entity\CurrencyOrder;
use App\Entity\ExchangeRate;
use App\Entity\Surcharge;
use App\Form\CurrencyOrderType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
    	$order             	= new CurrencyOrder();
    	$em                 = $this->getDoctrine()->getManager();
        $currencies         = $em->getRepository(Currency::class)->findAll();

        $allCurrencies      = [];
        $allRates 			= [];

        foreach ($currencies as $key => $currency) {
            $allCurrencies[$currency->getCode()] = $currency->getId();
        }

        $form           = $this->createForm(CurrencyOrderType::class, $order, array(
            'choices'   => $allCurrencies
        ));

        return $this->render('index/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/ajax-fetch/{id}", name="fetch_ajax")
     * @Method({"GET"})
     */
	public function ajaxAction(Request $request) { 
		$routeParams 	= $request->attributes->get('_route_params');
		$em 			= $this->getDoctrine()->getManager();
		$rate 			= $em->getRepository(ExchangeRate::class)->findOneBy(['currenySource'=>$routeParams['id']]);
		$surcharge 		= $em->getRepository(Surcharge::class)->findOneBy(['currencyId'=>$routeParams['id']]);

	   if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {  
			$jsonData = [
				'rate' => $rate->getRate(),  
				'surcharge' => $surcharge->getSurchage() 
			]; 

	    	return new JsonResponse($jsonData);

	   } else { 
	      return $this->render('index/index.html.twig'); 
	   } 
	}         
}
