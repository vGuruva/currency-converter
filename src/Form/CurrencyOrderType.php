<?php

namespace App\Form;

use App\Entity\CurrencyOrder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CurrencyOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currencyId', ChoiceType::class, array(
                'placeholder'       => 'Select currency',
                'choices'           => $options['choices']
            ))

            ->add('currencyAmount', TextType::class)
            ->add('payoutAmount', TextType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CurrencyOrder::class,
            'choices'  => null
        ]);
    }
}
