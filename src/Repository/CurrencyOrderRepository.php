<?php

namespace App\Repository;

use App\Entity\CurrencyOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CurrencyOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method CurrencyOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method CurrencyOrder[]    findAll()
 * @method CurrencyOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CurrencyOrderRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CurrencyOrder::class);
    }

//    /**
//     * @return CurrencyOrder[] Returns an array of CurrencyOrder objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CurrencyOrder
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
