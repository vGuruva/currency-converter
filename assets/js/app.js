/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

var currencyAmount 			= $('#currency_order_currencyAmount');
var payoutAmount 			= $('#currency_order_payoutAmount');
var exchRate 				= $('.exchRate');
var surChargePercentage 	= $('.surChargePercentage');
var surChargeAmount 		= $('.surChargeAmount');
var global 					= {};
var totalCost				= $('.totalCost');
var currencyAcquired		= $('.currencyAcquired');
  
$("#currency_order_currencyId").on("change", function(event){
	var currency = $(this).find(":selected").text();

	if (currency == 'USD') return;
	if (currency == 'Select currency') return;

	$.ajax({  
	   url:        '/ajax-fetch/'+ $(this).val(),  
	   type:       'POST',   
	   dataType:   'json',  
	   async:      true,	   
	   success: function(data, status) {  
	   		var calculatedSurcharge = data.surcharge*(1/data.rate);
			currencyAmount.val(1);
			currencyAcquired.html(currency+ ' '+ 1 + '.00');
			totalCost.html('USD '+ ((1/data.rate) + calculatedSurcharge));
			payoutAmount.val(1/data.rate); 
			exchRate.html(currency+' '+data.rate+' to USD 1.00'); 
			surChargePercentage.html( (data.surcharge*100)+'%' ); 
			surChargeAmount.html('USD ' + calculatedSurcharge);
			
			return new Promise(function(resolve, reject) {
				global.rateToUse 	= data.rate;
				global.currency 	= currency;
				global.surcharge 	= data.surcharge;
			  	resolve(global);
			});
	   },  
	   error : function(xhr, textStatus, errorThrown) {  
	      alert('Ajax request failed.');  
	   }  
	});  
}); 

$("#currency_order_payoutAmount").on("keyup change", function(event){	
	var surCharge = global.surcharge*$(this).val();
	var amount = $(this).val()*global.rateToUse;
	var totals = parseInt($(this).val()) + parseInt(surCharge);

	currencyAmount.val(amount);
	surChargeAmount.html('USD ' + surCharge);
	totalCost.html('USD '+ totals);
	currencyAcquired.html(global.currency+amount);

	//update gloabl variable for saving **checked**
	// console.log('payOut', $(this).val());
	// console.log('rate', global.rateToUse);
	// console.log('surCharge rate', global.surcharge);
	// console.log('surCharge', surCharge);
}); 

$("#currency_order_currencyAmount").on("keyup change", function(event){
	var payOut = $(this).val()/global.rateToUse;
	payoutAmount.val(payOut);
	surChargeAmount.html('USD ' +(global.surcharge*payOut));
	totalCost.html('USD '+ ((1 + global.surcharge)*payOut));
	currencyAcquired.html(global.currency+$(this).val());
	//update gloabl variable for saving
}); 
  

